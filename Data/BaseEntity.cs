﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadsUp.Data
{
    public class BaseEntity<TIdentifier> : IEntity<TIdentifier>
    {
        public TIdentifier Id { get; set; }

        public bool DeletedFlag { get; protected set; }

        public DateTime CreatedOnDateUtc { get; internal set; }

        public BaseEntity()
        {
            this.Id = default(TIdentifier);
            this.CreatedOnDateUtc = DateTime.UtcNow;
        }

        public void MarkEntityAsDeleted()
        {
            this.DeletedFlag = true;
        }
    }
}

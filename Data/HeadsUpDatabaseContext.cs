﻿using System.Data.Entity;
using System.Reflection;
using HeadsUp.Data.Mappings;
using HeadsUp.Data.Models;
using SQLite.CodeFirst;

namespace HeadsUp.Data
{
    public class HeadsUpDatabaseContext : DbContext
    {
        private const string DefaultProvider = "System.Data.SQLite.EF6";

        public DbSet<IndexSource> PsiGroupResults { get; set; }
        public DbSet<IndexDefinition> PsiGroups { get; set; }
        public DbSet<IndexCalculations> Psi { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //This bit is code first
            var coreMappingType = typeof(PsiGroupResultMapping);
            var coreAssembly = Assembly.GetAssembly(coreMappingType);
            modelBuilder.Configurations.AddFromAssembly(coreAssembly);

            var sqliteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<HeadsUpDatabaseContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);
        }
    }
}

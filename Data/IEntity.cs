﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeadsUp.Data
{
    public interface IEntity<TIdentifier>
    {
        DateTime CreatedOnDateUtc { get; }
        TIdentifier Id { get; set; }
        bool DeletedFlag { get; }
        void MarkEntityAsDeleted();
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HeadsUp.Data.Models;

namespace HeadsUp.Data.Mappings
{
    public class PsiGroupMapping : EntityTypeConfiguration<IndexDefinition>
    {
        public PsiGroupMapping()
        {
            this.ToTable("PSIGroup");
            this.HasKey(table => table.Id);
            this.Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");
            this.HasMany(table => table.IndexSourceData);
            this.HasMany(table => table.CalculatedIndex);
        }
    }
}

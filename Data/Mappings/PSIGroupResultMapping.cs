﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HeadsUp.Data.Models;

namespace HeadsUp.Data.Mappings
{
    class PsiGroupResultMapping : EntityTypeConfiguration<IndexSource>
    {
        public PsiGroupResultMapping()
        {
            this.ToTable("PSIGroupResult");
            this.HasKey(table => table.Id);
            this.Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");

            this.HasRequired(table => table.IndexDefinition);
        }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using HeadsUp.Data.Models;


namespace HeadsUp.Data.Mappings
{
    class PopulationStabilityIndexMapping : EntityTypeConfiguration<IndexCalculations>
    {
        public PopulationStabilityIndexMapping()
        {
            this.ToTable("IndexCalculations");
            this.HasKey(table => table.Id);
            this.Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnName("Id");

            this.HasRequired(table => table.IndexDefinition);
        }
    }
}

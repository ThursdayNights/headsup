﻿using HeadsUp.Data;
using HeadsUp.Data.Models;

namespace HeadsUp.Data.Models
{
    public class IndexCalculations : BaseEntity<int>
    {
        public string Cluster { get; set; }
        public int BaseDateDimentionRollUp { get; set; }
        public int TestDateDimentionRollUp { get; set; }
        public decimal BaseMeasureIndexValue { get; set; }
        public decimal TestMeasureIndexValue { get; set; }
        public decimal FinalMeasureIndexValue { get; set; }
        public decimal BaseRowCountIndexValue { get; set; }
        public decimal TestRowCountIndexValue { get; set; }
        public decimal FinalRowCountIndexValue { get; set; }

        public virtual IndexDefinition IndexDefinition { get; set; }
    }
}

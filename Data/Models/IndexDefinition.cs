﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;

namespace HeadsUp.Data.Models
{
    public class IndexDefinition : BaseEntity<int>
    {
        [Index("IX_UniqueName", 1, IsUnique = true)]
        public string IndexName { get; set; }

        [Index("IX_UniqueDefinition", 1, IsUnique = true)]
        public string DatabaseServerName { get; set; }
        [Index("IX_UniqueDefinition", 2, IsUnique = true)]
        public string DatabaseName { get; set; }
        [Index("IX_UniqueDefinition", 3, IsUnique = true)]
        public string DatabaseTableName { get; set; }
        public string SqlQueryScript { get; set; }
        [Index("IX_UniqueDefinition", 4, IsUnique = true)]
        public string Cluster { get; set; }
        [Index("IX_UniqueDefinition", 5, IsUnique = true)]
        public string Measure { get; set; }
        [Index("IX_UniqueDefinition", 6, IsUnique = true)]
        public string DateDimension { get; set; }

        public List<IndexSource> IndexSourceData { get; set; }
        public List<IndexCalculations> CalculatedIndex { get; set; }

        public IndexDefinition(string databaseServerName, string databaseName, string tableName, string dimension, string measure, string dateDimension)
        {
            DatabaseServerName = databaseServerName;
            DatabaseName = databaseName;
            DatabaseTableName = tableName;
            Cluster = dimension;
            Measure = measure;
            DateDimension = dateDimension;
            IndexSourceData = new List<IndexSource>();
            CalculatedIndex = new List<IndexCalculations>();
        }
        public IndexDefinition()
        {
            this.IndexSourceData = new List<IndexSource>();
            this.CalculatedIndex = new List<IndexCalculations>();
        }

        public static void StoreIntermediateGroupResults(IndexDefinition indexDefinition, List<IndexSource> psiGroupResultList)
        {

            using (var dbContext = new HeadsUpDatabaseContext())
            {
                dbContext.PsiGroups.Attach(indexDefinition);
                indexDefinition.IndexSourceData.AddRange(psiGroupResultList);
                dbContext.SaveChanges();
            }
        }

        public static IndexDefinition StorePsiGroup(IndexDefinition indexDefinition)
        {
            if (indexDefinition == null) throw new ArgumentNullException(nameof(IndexDefinition));
            using (var dbContext = new HeadsUpDatabaseContext())
            {

                dbContext.PsiGroups.Where(b => b.DatabaseServerName.Equals(indexDefinition.DatabaseServerName))
                                    .Where(b => b.DatabaseName.Equals(indexDefinition.DatabaseName))
                                    .Where(b => b.DatabaseTableName.Equals(indexDefinition.DatabaseTableName))
                                    .Where(b => b.Cluster.Equals(indexDefinition.Cluster))
                                    .Where(b => b.Measure.Equals(indexDefinition.Measure))
                                    .Where(b => b.DateDimension.Equals(indexDefinition.DateDimension));
                if (!dbContext.PsiGroups.Any())
                {
                    dbContext.PsiGroups.Add(indexDefinition);
                }
                else
                {
                    dbContext.PsiGroups.Attach(indexDefinition);
                }
                
                
                dbContext.SaveChanges();
            }
            return indexDefinition;
        }

        public static void StoreFinalIndex(IndexDefinition indexDefinition, List<IndexCalculations> finalIndex)
        {
            if (indexDefinition == null) throw new ArgumentNullException(nameof(indexDefinition));
            if (finalIndex == null) throw new ArgumentNullException(nameof(finalIndex));

            using (var dbContext = new HeadsUpDatabaseContext())
            {
                dbContext.PsiGroups.Attach(indexDefinition);
                indexDefinition.CalculatedIndex.AddRange(finalIndex);
                dbContext.SaveChanges();
            }
        }

        public IndexDefinition RetrievePsiGroup(IndexDefinition indexDefinition)
        {
            using (var dbContext = new HeadsUpDatabaseContext())
            {
                var blogs =
                    dbContext.PsiGroups.Where(b => b.DatabaseServerName.Equals(indexDefinition.DatabaseServerName))
                        .Where(b => b.DatabaseName.Equals(indexDefinition.DatabaseName))
                        .Where(b => b.DatabaseTableName.Equals(indexDefinition.DatabaseTableName))
                        .Where(b => b.Cluster.Equals(indexDefinition.Cluster))
                        .Where(b => b.Measure.Equals(indexDefinition.Measure))
                        .Where(b => b.DateDimension.Equals(indexDefinition.DateDimension));
                
                return indexDefinition;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;

namespace HeadsUp.Data.Models
{
    public class IndexSource : BaseEntity<int>
    {
        [Index]
        public string Cluster { get; set; }
        [Index]
        public int DateDimentionRollUp { get; set; }
        public int ClusterRowCount { get; set; }
        public int ClusterRowCountTotal { get; set; }
        public decimal SumOfMeasure { get; set; }
        public decimal SumOfMeasureTotal { get; set; }
        //todo rename these variables
        public decimal POfMeasure { get; set; }
        public decimal POfRowCount { get; set; }
        public virtual IndexDefinition IndexDefinition { get; set; }


        /// <exception cref="DbUpdateException">An error occurred sending updates to the database.</exception>
        /// <exception cref="DbUpdateConcurrencyException">
        ///             A database command did not affect the expected number of rows. This usually indicates an optimistic 
        ///             concurrency violation; that is, a row has been changed in the database since it was queried.
        ///             </exception>
        /// <exception cref="DbEntityValidationException">
        ///             The save was aborted because validation of entity property values failed.
        ///             </exception>
        /// <exception cref="NotSupportedException">
        ///             An attempt was made to use unsupported behavior such as executing multiple asynchronous commands concurrently
        ///             on the same context instance.</exception>
        /// <exception cref="ObjectDisposedException">The context or connection have been disposed.</exception>
        /// <exception cref="InvalidOperationException">
        ///             Some error occurred attempting to process entities in the context either before or after sending commands
        ///             to the database.
        ///             </exception>
        public static void StorePsiGroupResults(IndexDefinition indexDefinition, List<IndexSource> psiGroupResultList)
        {
            using (var dbContext = new HeadsUpDatabaseContext())
            {
                dbContext.PsiGroupResults.AddRange(psiGroupResultList);
                dbContext.SaveChanges();
            }
        }

        /// <exception cref="InvalidOperationException">Thrown if multiple entities exist in the context with the primary key values given.</exception>
        public static void RetrievePsiGroupResults()
        {
            using (var dbContext = new HeadsUpDatabaseContext())
            {
                dbContext.PsiGroupResults.Find();
            }
        }
    }


}

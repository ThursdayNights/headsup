﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeadsUp.Data;

namespace HeadsUp.Api.Models
{
    public class Index : BaseEntity<int>
    {
        public string Cluster { get; set; }
        public int BaseDateDimentionRollUp { get; set; }
        public int TestDateDimentionRollUp { get; set; }
        public decimal BaseMeasureIndexValue { get; set; }
        public decimal TestMeasureIndexValue { get; set; }
        public decimal FinalMeasureIndexValue { get; set; }
        public decimal BaseRowCountIndexValue { get; set; }
        public decimal TestRowCountIndexValue { get; set; }
        public decimal FinalRowCountIndexValue { get; set; }

        //  public virtual Api.Models.IndexDefinition IndexDefinition { get; set; }

        public Index(Data.Models.IndexCalculations inputIndexCalculations)
        {
            this.Cluster = inputIndexCalculations.Cluster;
            this.BaseDateDimentionRollUp = inputIndexCalculations.BaseDateDimentionRollUp;
            this.BaseMeasureIndexValue = inputIndexCalculations.BaseMeasureIndexValue;
            this.BaseRowCountIndexValue = inputIndexCalculations.BaseRowCountIndexValue;
            this.FinalMeasureIndexValue = inputIndexCalculations.FinalMeasureIndexValue;
            this.FinalRowCountIndexValue = inputIndexCalculations.FinalRowCountIndexValue;
            this.TestDateDimentionRollUp = inputIndexCalculations.TestDateDimentionRollUp;
            this.TestMeasureIndexValue = inputIndexCalculations.TestMeasureIndexValue;
            this.TestRowCountIndexValue = inputIndexCalculations.TestRowCountIndexValue;
        }

        public Index()
        {
        }
    }
}

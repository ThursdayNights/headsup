﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeadsUp.Data;
using HeadsUp.Data.Models;

namespace HeadsUp.Api.Models
{
    public class IndexDefinition : BaseEntity<int>
    {
        public string IndexName { get; set; }
        public string DatabaseServerName { get; set; }
        public string DatabaseName { get; set; }
        public string DatabaseTableName { get; set; }
        public string SqlQueryScript { get; set; }
        public string Cluster { get; set; }
        public string Measure { get; set; }
        public string DateDimension { get; set; }

        //public List<IndexSource> _IndexSourceData { get; set; }
        //public List<Index> Index { get; set; }

        public IndexDefinition(string indexName, string databaseServerName, string databaseName, string databaseTableName, string cluster, string measure, string dateDimension)
        {
            Cluster = cluster;
            DatabaseName = databaseName;
            DatabaseServerName = databaseServerName;
            DatabaseTableName = databaseTableName;
            DateDimension = dateDimension;
            Measure = measure;
            SqlQueryScript = "";
        }

        public IndexDefinition()
        {
        }
    }
}
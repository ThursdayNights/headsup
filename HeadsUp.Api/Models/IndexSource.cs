﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeadsUp.Data;

namespace HeadsUp.Api.Models
{
    public class IndexSource : BaseEntity<int>
    {
        public string Cluster { get; set; }
        public int DateDimentionRollUp { get; set; }
        public int ClusterRowCount { get; set; }
        public int ClusterRowCountTotal { get; set; }
        public decimal SumOfMeasure { get; set; }
        public decimal SumOfMeasureTotal { get; set; }
        //todo rename these variables
        public decimal POfMeasure { get; set; }
        public decimal POfRowCount { get; set; }

        // public virtual HeadsUp.Api.Models.IndexDefinition { get; set; }


        public IndexSource(Data.Models.IndexSource inputIndexSource)
        {
            Cluster = inputIndexSource.Cluster;
            ClusterRowCount = inputIndexSource.ClusterRowCount;
            ClusterRowCountTotal = inputIndexSource.ClusterRowCountTotal;
            DateDimentionRollUp = inputIndexSource.DateDimentionRollUp;
            POfMeasure = inputIndexSource.POfMeasure;
            POfRowCount = inputIndexSource.POfRowCount;
            SumOfMeasure = inputIndexSource.SumOfMeasure;
            SumOfMeasureTotal = inputIndexSource.SumOfMeasureTotal;
        }
        public IndexSource()
        {
        }
    }
}

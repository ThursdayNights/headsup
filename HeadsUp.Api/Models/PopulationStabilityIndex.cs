﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using HeadsUp.Common;

namespace HeadsUp.Api.Models
{
    public class PopulationStabilityIndex
    {
        public IndexDefinition IndexDefinition { get; set; }
        public List<IndexSource> IndexSources;
        public List<Index> Indices;

        public PopulationStabilityIndex(IndexDefinition indexDefinition, List<IndexSource> indexSources, List<Index> indices)
        {
            IndexDefinition = indexDefinition;
            IndexSources = indexSources;
            Indices = indices;
        }

        public PopulationStabilityIndex()
        {
            IndexDefinition = new IndexDefinition();
            IndexSources = new List<IndexSource>();
            Indices = new List<Index>();
        }
        public PopulationStabilityIndex(string indexName, string databaseServerName, string databaseName, string tableName, string dimension, string measure, string dateDimension)
        {
            IndexDefinition = new IndexDefinition(indexName, databaseServerName, databaseName, tableName, dimension, measure, dateDimension);

            var resultsTable = Integration.Query.GetIndexSourceData(IndexDefinition.DatabaseServerName,
                IndexDefinition.DatabaseName, IndexDefinition.DatabaseTableName, IndexDefinition.Cluster,
                IndexDefinition.Measure, IndexDefinition.DateDimension);
            IndexSources = resultsTable.DataTableToList<IndexSource>();

            Indices = CalculateIndex(IndexSources);
        }


        public void PopulateIndexSourceData()
        {

        }

        public void CompareBaseAndTest(int baseDateDimentionRollUp, int testDateDimentionRollUp)
        {
            if (baseDateDimentionRollUp <= 190001) throw new ArgumentOutOfRangeException(nameof(baseDateDimentionRollUp));
            if (testDateDimentionRollUp <= 190001) throw new ArgumentOutOfRangeException(nameof(testDateDimentionRollUp));

        }

        public static List<Index> CalculateIndex(List<IndexSource> indexSource)
        {
            return CalculateIndex(indexSource, indexSource);
        }


        public static List<Index> CalculateIndex(List<IndexSource> baseIndexSource, List<IndexSource> testIndexSource)
        {
            if (baseIndexSource == null) throw new ArgumentNullException(nameof(baseIndexSource));
            if (testIndexSource == null) throw new ArgumentNullException(nameof(testIndexSource));


            var listOfBaseClusters = new string[baseIndexSource.Count];
            for (var i = 0; i < baseIndexSource.Count; i++)
            {
                listOfBaseClusters[i] = baseIndexSource[i].Cluster;
            }


            var listOfTestClusters = new string[testIndexSource.Count];
            for (var j = 0; j < testIndexSource.Count; j++)
            {
                listOfTestClusters[j] = testIndexSource[j].Cluster;
            }

            var distinctClusters = listOfBaseClusters.Concat(listOfTestClusters).ToArray();
            distinctClusters = distinctClusters.Distinct().ToArray();


            var listOfBaseDateDimensions = new int[baseIndexSource.Count];
            for (var i = 0; i < baseIndexSource.Count; i++)
            {
                listOfBaseDateDimensions[i] = baseIndexSource[i].DateDimentionRollUp;
            }


            var listOfTestDateDimensions = new int[baseIndexSource.Count];
            for (var i = 0; i < baseIndexSource.Count; i++)
            {
                listOfTestDateDimensions[i] = baseIndexSource[i].DateDimentionRollUp;
            }

            var distinctDateDimentionRollUp = listOfBaseDateDimensions.Concat(listOfTestDateDimensions).ToArray();
            distinctDateDimentionRollUp = distinctDateDimentionRollUp.Distinct().ToArray();


            var indices = new List<Index>();


            foreach (var cluster in distinctClusters)
            {
                foreach (var baseDateRollup in distinctDateDimentionRollUp)
                {
                    foreach (var testDateRollup in distinctDateDimentionRollUp)
                    {
                        if (baseDateRollup < testDateRollup)
                        {
                            var baseFind = baseIndexSource.FirstOrDefault(x => x.Cluster == cluster & x.DateDimentionRollUp == baseDateRollup);
                            if (baseFind != null)
                            {
                                var testFind = testIndexSource.FirstOrDefault(x => x.Cluster == cluster & x.DateDimentionRollUp == testDateRollup);
                                if (testFind != null)
                                {
                                    var index = new Index()
                                    {
                                        Cluster = cluster,
                                        BaseDateDimentionRollUp = baseFind.DateDimentionRollUp,
                                        TestDateDimentionRollUp = testFind.DateDimentionRollUp,
                                        BaseMeasureIndexValue = baseFind.POfMeasure,
                                        BaseRowCountIndexValue = baseFind.POfRowCount,
                                        TestMeasureIndexValue = testFind.POfMeasure,
                                        TestRowCountIndexValue = testFind.POfRowCount,
                                        FinalMeasureIndexValue =
                                            (decimal)
                                                ((double)(testFind.POfMeasure - baseFind.POfMeasure) *
                                                 (Math.Log10((double)(testFind.POfMeasure / baseFind.POfMeasure)))),
                                        FinalRowCountIndexValue =
                                            (decimal)
                                                ((double)(testFind.POfRowCount - baseFind.POfRowCount) *
                                                 (Math.Log10((double)(testFind.POfRowCount / baseFind.POfRowCount))))
                                    };


                                    indices.Add(index);
                                }
                            }
                        }
                    }
                }
            }
            return indices;
        }
    }
}

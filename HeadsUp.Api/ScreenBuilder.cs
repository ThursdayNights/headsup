﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using HeadsUp.Api.Models;
using HeadsUp.Common;
using HeadsUp.Integration;
using IndexDefinition = HeadsUp.Data.Models.IndexDefinition;
using IndexSource = HeadsUp.Data.Models.IndexSource;

namespace HeadsUp.Api
//todo rename the Class file from ScreenBuilder to something else
{
    public static class ScreenBuilder
    {
        public static List<string> GetDatabaseNames(string databaseServerName)
        {
            return Metadata.GetDatabaseNames(databaseServerName);
        }
        public static List<string> GetTableNames(string databaseServerName, string databaseName)
        {
            return Metadata.GetTableNames(databaseServerName, databaseName);
        }
        public static List<string> GetFieldNames(string databaseServerName, string databaseName, string tableName)
        {
            return Metadata.GetFieldNames(databaseServerName, databaseName, tableName);
        }
    }

    public static class PsiData
    {

        //public static PopulationStabilityIndex CalculatePsiIndex(string indexName, string databaseServerName, string databaseName, string tableName, string cluster, string measure, string dateDimension)
        //{
        //    //var populationStabilityIndex = new PopulationStabilityIndex(indexName,databaseServerName, databaseName, tableName, cluster, measure, dateDimension);
        //    //populationStabilityIndex.PopulateIndexSourceData();
        //    //populationStabilityIndex.CalculateIndex();



        //    //Create Query
        //    //Execute Query
        //    var resultsTable = Query.GetIndexSourceData(databaseServerName, databaseName, tableName, cluster, measure, dateDimension);
        //    //Transform result from DataTable to PSIGroup
        //    var resultList = resultsTable.DataTableToList<IndexSource>();
        //    //Store in SQLite
        //    var psiGroup = new IndexDefinition(databaseServerName, databaseName, tableName, cluster, measure, dateDimension);
        //    StorePsiGroup(psiGroup);
        //    psiGroup = RetrievePsiGroup(psiGroup);
        //    StoreIntermediateGroupResults(psiGroup, resultList);

        //    //remove this test code before Production
        //    var baseList = resultList.ToList();
        //   // baseList.RemoveAll(result => result.DateDimentionRollUp == 201602);

        //    var testList = resultList.ToList();
        //   // testList.RemoveAll(result => result.DateDimentionRollUp == 201601);
        //    //end of test code
        //    //-------------------------
        //    //var finalIndex = CalculateIndex(baseList, testList);
        //    //-------------------------

        //   // StoreFinalIndex(psiGroup, finalIndex);
        //    var returnFinalIndex = new List<Models.Index>();
        //    foreach (var eachResult in finalIndex)
        //    {
        //        var index = new Models.Index(eachResult);
        //        returnFinalIndex.Add(index);
        //    }
        //    var measureIndex = returnFinalIndex.Sum(x => x.FinalMeasureIndexValue);
        //    var rowIndex = returnFinalIndex.Sum(x => x.FinalRowCountIndexValue);

        //    //StorePSIGroupResults(IndexDefinition,resultList);
        //    // RetrievePSIResults();
        //    //Retrieve results from SQLite
        //    //Return results to calling object
        //    return populationStabilityIndex;
        //}

        public static IndexDefinition RetrievePsiGroup(IndexDefinition indexDefinition)
        {
            return indexDefinition.RetrievePsiGroup(indexDefinition);
        }

        public static void StorePsiGroup(IndexDefinition indexDefinition)
        {
            IndexDefinition.StorePsiGroup(indexDefinition);
        }

        public static void StorePsiGroupResults(IndexDefinition indexDefinition, List<IndexSource> psiGroupResultList)
        {
            IndexSource.StorePsiGroupResults(indexDefinition, psiGroupResultList);
        }


        public static void StoreIntermediateGroupResults(IndexDefinition indexDefinition, List<IndexSource> psiGroupResultList)
        {
            IndexDefinition.StoreIntermediateGroupResults(indexDefinition, psiGroupResultList);
        }

        private static void StoreFinalIndex(IndexDefinition indexDefinition, List<Data.Models.IndexCalculations> finalIndex)
        {
            IndexDefinition.StoreFinalIndex(indexDefinition, finalIndex);

        }





 
    }
}

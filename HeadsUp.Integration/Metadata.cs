﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace HeadsUp.Integration
{
    public class Query
    {
        //todo rename this method - ugly name
        /// <exception cref="InvalidOperationException">Cannot open a connection without specifying a data source or server.orThe connection is already open.</exception>
        /// <exception cref="ArgumentException">An invalid connection string argument has been supplied, or a required connection string argument has not been supplied. </exception>
        public static DataTable GetIndexSourceData(string databaseServer, string databaseName, string tableName, string dimension, string measure, string dateDimension)
        {
            using (var sqlConn = new SqlConnection())
            {
                // List<string> fieldList = new List<string>();
                var sqlConnString = new SqlConnectionStringBuilder
                {
                    DataSource = databaseServer,
                    InitialCatalog = databaseName,
                    IntegratedSecurity = true
                };
                sqlConn.ConnectionString = sqlConnString.ToString();
                //string queryString = "WITH uniqueDimension (Dimension) AS (select distinct " + dimension + " from TestData_CSV1 t) select " + dimension + " dimension," + dateDimension + " monthNumber, count(*) as dimensionRowCount, sum(convert(numeric, " + measure + ")) measureSum from uniqueDimension ud left outer join " + tableName + " t on ud.dimension = t." + dimension + " group by " + dimension + ", " + dateDimension + " order by " + dateDimension + ", " + dimension + "";
                var queryString = "WITH uniqueDimensionCTE( Dimension) AS (SELECT DISTINCT " + dimension + " FROM " + tableName + " AS t), sumCTE as ( SELECT " + dimension + " AS dimension , " + dateDimension + " AS monthNumber , COUNT(*) AS dimensionRowCount , SUM(CONVERT( Numeric, " + measure + ")) AS measureSum FROM uniqueDimensionCTE AS ud LEFT OUTER JOIN " + tableName + " AS t ON ud.dimension=t." + dimension + " GROUP BY " + dimension + " , " + dateDimension + "),	 measureSumCTE (monthNumber,measureSumTotal,dimensionRowCountTotal) as ( select monthNumber,sum(measureSum)*1.0,sum(dimensionRowCount)*1.0 from sumCTE group by monthNumber) , P as ( select sumCTE.*,cte.measureSumTotal,cte.dimensionRowCountTotal,sumCTE.measureSum/cte.measureSumTotal as measureP,sumCTE.dimensionRowCount/cte.dimensionRowCountTotal as rowCountP from measureSumCTE cte left outer join sumCTE on cte.monthNumber = sumCTE.monthNumber) select dimension as Cluster,dimensionRowCount as ClusterRowCount,dimensionRowCountTotal as ClusterRowCountTotal,monthNumber as DateDimentionRollUp,measureP as POfMeasure,rowCountP as POfRowCount,measureSum as SumOfMeasure,measureSumTotal as SumOfMeasureTotal from p order by monthNumber,dimension";

                sqlConn.Open();

                var resultsTable = new DataTable();
                var sqlDataAdapter = new SqlDataAdapter(queryString, sqlConn);

                sqlDataAdapter.Fill(resultsTable);
                return resultsTable;
            }
        }
    }
    public class Metadata
    {
        public static List<string> GetDatabaseNames(string databaseServer)
        {
            List<string> databaseList = new List<string>();
            SqlConnectionStringBuilder sqlConnString = new SqlConnectionStringBuilder();
            sqlConnString.DataSource = databaseServer;
            sqlConnString.InitialCatalog = "master";
            sqlConnString.IntegratedSecurity = true;

            string connString = sqlConnString.ToString();

            string queryString = "SELECT name from sys.databases";

            using (SqlConnection connection = new SqlConnection(connString))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = queryString;

                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        databaseList.Add(reader[0].ToString());
                    }
                }
            }
            return databaseList;
        }

        public static List<string> GetTableNames(string databaseServer, string databaseName)
        {
            List<string> tableList = new List<string>();
            SqlConnectionStringBuilder sqlConnString = new SqlConnectionStringBuilder();
            sqlConnString.DataSource = databaseServer;
            sqlConnString.InitialCatalog = databaseName;
            sqlConnString.IntegratedSecurity = true;

            string connString = sqlConnString.ToString();

            string queryString = "SELECT TABLE_NAME FROM " + databaseName + ".INFORMATION_SCHEMA.Tables ";

            using (SqlConnection connection = new SqlConnection(connString))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = queryString;

                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tableList.Add(reader[0].ToString());
                    }
                }
            }
            return tableList;
        }

        public static DataTable GetAllFieldInfo(string databaseServer, string databaseName, string tableName)
        {
            using (SqlConnection sqlConn = new SqlConnection())
            {
                // List<string> fieldList = new List<string>();
                SqlConnectionStringBuilder sqlConnString = new SqlConnectionStringBuilder();
                sqlConnString.DataSource = databaseServer;
                sqlConnString.InitialCatalog = databaseName;
                sqlConnString.IntegratedSecurity = true;

                sqlConn.ConnectionString = sqlConnString.ToString();
                string queryString = "SELECT ORDINAL_POSITION, COLUMN_NAME, IS_NULLABLE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION,DATETIME_PRECISION FROM " + databaseName + ".INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tableName + "'";
                sqlConn.Open();

                DataTable table = new DataTable();
                SqlDataAdapter a = new SqlDataAdapter(queryString, sqlConn);
                a.Fill(table);
                return table;

            }

        }

        public static List<string> GetFieldNames(string databaseServer, string databaseName, string tableName)
        {
            List<string> fieldList = new List<string>();
            SqlConnectionStringBuilder sqlConnString = new SqlConnectionStringBuilder();
            sqlConnString.DataSource = databaseServer;
            sqlConnString.InitialCatalog = databaseName;
            sqlConnString.IntegratedSecurity = true;

            string connString = sqlConnString.ToString();

            string queryString = "SELECT COLUMN_NAME FROM " + databaseName + ".INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" + tableName + "'";

            using (SqlConnection connection = new SqlConnection(connString))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = queryString;

                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        fieldList.Add(reader[0].ToString());
                    }
                }
            }
            return fieldList;
        }
    }
}

﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MahApps.Metro.Controls;
using HeadsUp.Api;
using HeadsUp.Api.Models;
using static HeadsUp.Api.PsiData;


namespace HeadsUp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void connectToDatabaseServerButton_Click(object sender, RoutedEventArgs e)
        {
            
            InitialiseDatabaseComboBox();
           
        }
        public void InitialiseDatabaseComboBox()
        {
            HeadsUpProgressBar.IsIndeterminate = true;
            DatabaseListComboBox.Items.Clear();
            List<string> databaseList = ScreenBuilder.GetDatabaseNames(DatabaseServerTextBox.Text);
            foreach (var item in databaseList)
            {
                DatabaseListComboBox.Items.Add(item);
            }
            DatabaseListComboBox.SelectedIndex = 0;
            HeadsUpProgressBar.IsIndeterminate = false;
        }
        private void databaseListComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           // InitializeTableComboBox();
        }
        private void databaseListComboBox_DropDownClosed(object sender, System.EventArgs e)
        {
            InitializeTableComboBox();
        }

        public void InitializeTableComboBox()
        {
            HeadsUpProgressBar.IsIndeterminate = true;
            TableListComboBox.Items.Clear();
            List<string> tableList = ScreenBuilder.GetTableNames(DatabaseServerTextBox.Text, DatabaseListComboBox.Text);
            foreach (var item in tableList)
            {
                TableListComboBox.Items.Add(item);
            }
            TableListComboBox.SelectedIndex = 0;
            HeadsUpProgressBar.IsIndeterminate = false;
        }

        public void InitialiseFieldComboBoxes()
        {
            HeadsUpProgressBar.IsIndeterminate = true;
            DimensionComboBox.Items.Clear();
            MeasureComboBox.Items.Clear();
            DateDimensionComboBox.Items.Clear();
            List<string> fieldList = ScreenBuilder.GetFieldNames(DatabaseServerTextBox.Text, DatabaseListComboBox.Text, TableListComboBox.Text);
            foreach (var item in fieldList)
            {
                DimensionComboBox.Items.Add(item);
                MeasureComboBox.Items.Add(item);
                DateDimensionComboBox.Items.Add(item);
            }
            DimensionComboBox.SelectedIndex = 0;
            MeasureComboBox.SelectedIndex = 0;
            DateDimensionComboBox.SelectedIndex = 0;

            HeadsUpProgressBar.IsIndeterminate = false;
        }


        private void databaseListComboBox_TextInput(object sender, TextCompositionEventArgs e)
        {
            InitializeTableComboBox();
        }

        private void getTables_Click(object sender, RoutedEventArgs e)
        {
            InitializeTableComboBox();
        }

        private void getFieldsButton_Click(object sender, RoutedEventArgs e)
        {
            //fieldsDataGrid.DataContext = GetFields(databaseServerTextBox.Text, databaseListComboBox.Text, tableListComboBox.Text);
            InitialiseFieldComboBoxes();
        }

        private void fieldsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            DataGrid dataGrid = (sender as DataGrid);
            
            //textBoxCellValue.Text = dataGrid.SelectedIndex.ToString();


            //{ System.Windows.Controls.DataGridCellInfo}
            //currentcell.column
            //    currentcell.isset
            //    currentcell.item
            //-		SelectedCells	{System.Windows.Controls.SelectedCellsCollection}	System.Collections.Generic.IList<System.Windows.Controls.DataGridCellInfo> {System.Windows.Controls.SelectedCellsCollection}
            //-		Results View	Expanding the Results View will enumerate the IEnumerable	
            //		SelectedIndex	2	int


            DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
            DataGridCell rowColumn = dataGrid.Columns[dataGrid.CurrentColumn.DisplayIndex].GetCellContent(row).Parent as DataGridCell;



            var cellValue = ((TextBlock)rowColumn.Content).Text;

        }

        private void PSIbutton_Click(object sender, RoutedEventArgs e)
        {
            HeadsUpProgressBar.IsIndeterminate = !HeadsUpProgressBar.IsIndeterminate;
        }

        public void runQueryButton_Click(object sender, RoutedEventArgs e)
        {
            HeadsUpProgressBar.IsIndeterminate = true;
            var populationStabilityIndex = new PopulationStabilityIndex(IndexName.Text, DatabaseServerTextBox.Text, DatabaseListComboBox.Text, TableListComboBox.Text, DimensionComboBox.Text, MeasureComboBox.Text, DateDimensionComboBox.Text);
            //frontend needs to be populated from here 
            
            //InitialDataGrid.DataContext = populationStabilityIndex.Indices;

            //InitialDataGrid.DataContext = CalculatePsiIndex(IndexName.Text,DatabaseServerTextBox.Text, DatabaseListComboBox.Text, TableListComboBox.Text, DimensionComboBox.Text, MeasureComboBox.Text, DateDimensionComboBox.Text);

            //SaveSampleData(databaseServerTextBox.Text, databaseListComboBox.Text, tableListComboBox.Text, dimensionComboBox.Text, measureComboBox.Text, dateDimensionComboBox.Text);
            //HeadsUpProgressBar.IsIndeterminate = false;
        }

        private void tableListComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InitialiseFieldComboBoxes();
        }

        private void tableListComboBox_DropDownClosed(object sender, System.EventArgs e)
        {
            InitialiseFieldComboBoxes();
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
